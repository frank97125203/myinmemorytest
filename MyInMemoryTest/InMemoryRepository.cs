﻿using Microsoft.EntityFrameworkCore;
using MyInMemoryTest.Production;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyInMemoryTest
{
    public class InMemoryRepository
    {
        public EmployeeContext employeeContext;
        public IEmployeeRepository Get()
        {
            var options = new DbContextOptionsBuilder<EmployeeContext>().UseInMemoryDatabase(databaseName: "EmpDB").Options;

            employeeContext = new EmployeeContext(options);

            var repository = new EmployeeRepository(employeeContext);

            return repository;
        }
    }
}
