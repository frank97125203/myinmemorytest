using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using MyInMemoryTest.Production;
using MyInMemoryTest.Production.Entities;
using NUnit.Framework;

namespace MyInMemoryTest
{
    public class EmployeeRepositoryTests
    {
        private IEmployeeRepository _employeeRepository;
        private EmployeeContext _employeeContext;
        private InMemoryRepository _inMemoryRepository;
        [SetUp]
        public void Setup()
        {
            _inMemoryRepository = new InMemoryRepository();
            this._employeeRepository = _inMemoryRepository.Get();
        }

        [Test]
        public async Task add_a_employee()
        {
            EmployeeDto dto = new EmployeeDto() { Department = DepartmentEnum.火箭隊與她快樂夥伴, FirstName = "陳", LastName = "宇鋒" };

            await _employeeRepository.AddAsync(dto);

            _inMemoryRepository.employeeContext.Employees.Count().Should().Be(1);
            _inMemoryRepository.employeeContext.Employees.Where(x => x.DepartmentId == 2).First().Name.Should().Be("陳 宇鋒");
        }

        [Test]
        public async Task find_海外企業開發組()
        {
            var emp = await _employeeRepository.FindByDepartmentId(DepartmentEnum.海外企業開發組);
            emp.Count().Should().Be(0);

        } 
    }
}